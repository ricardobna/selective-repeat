import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Scanner;


public class Client {
	public static double PROBABILIDADE_DE_DESCARTE = 0.5;
	public static int TAMANHO_DO_PACOTE = 100;
	private static boolean bufferVazio = true;

	public static void main(String[] args) throws Exception{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Diga a probabilidade de descarte de pacotes pelo cliente: ");
		PROBABILIDADE_DE_DESCARTE = scanner.nextDouble();
		scanner.close();
		DatagramSocket socketUdp = new DatagramSocket(35000);
		byte[] dadoRecebido = new byte[TAMANHO_DO_PACOTE];
		int pacoteEsperado = 0;
		ArrayList<Pacote> pacotesRecebidos = new ArrayList<Pacote>();
		boolean end = false;
		ArrayList<Pacote> bufferDePacotes = new ArrayList<Pacote>();
		
		while (!end) {
			System.out.println("Esperando pacote...");
			DatagramPacket pacoteRecebido = new DatagramPacket(dadoRecebido, dadoRecebido.length);
			socketUdp.receive(pacoteRecebido);
			Pacote pacote = null;
			try {
				pacote = (Pacote) Convert.toObject(pacoteRecebido.getData());
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Pacote de numero " + pacote.getNum() + " recebido, " + (pacote.isUltimo() ? "e o ultimo." : "nao e o ultimo."));
		
			if (pacote.getNum() == pacoteEsperado) {
				pacotesRecebidos.add(pacote);
				if(pacote.isUltimo()) {
					System.out.println("Ultimo pacote recebido.");
				}
				Confirmacao objetoAck = new Confirmacao(pacoteEsperado, pacote.isUltimo(), true);
				byte[] ackBytes = Convert.toBytes(objetoAck);
				DatagramPacket pacoteAck = new DatagramPacket(ackBytes, ackBytes.length, pacoteRecebido.getAddress(), pacoteRecebido.getPort());
				if (Math.random() > PROBABILIDADE_DE_DESCARTE) {
					socketUdp.send(pacoteAck);
					pacoteEsperado++;
					System.out.println("Mandando ACK do pacote de numero " + pacoteEsperado + " com  " + ackBytes.length  + " bytes.");
				} else {
					System.out.println("ACK do pacote de numero " + objetoAck.getNumDoPacote() + " foi perdido.");
				}
				if(!bufferVazio) {
					for(Pacote pacotes : bufferDePacotes) {
						pacotesRecebidos.add(pacotes);
						Confirmacao ackDoPacoteDoBuffer = new Confirmacao(pacoteEsperado, pacotes.isUltimo(), true);
						byte[] byteDoAckDoPacoteDoBuffer = Convert.toBytes(ackDoPacoteDoBuffer);
						DatagramPacket pacoteDoAckDoPacoteNoBuffer = new DatagramPacket(byteDoAckDoPacoteDoBuffer, byteDoAckDoPacoteDoBuffer.length, pacoteRecebido.getAddress(), pacoteRecebido.getPort());
						if (Math.random() > PROBABILIDADE_DE_DESCARTE) {
							socketUdp.send(pacoteDoAckDoPacoteNoBuffer);
							pacoteEsperado++;
							System.out.println("Mandando ACK do pacote de numero " + pacoteEsperado + " com " + ackBytes.length  + " bytes.");
						} else {
							System.out.println("ACK do pacote de numero " + objetoAck.getNumDoPacote() + " foi perdido.");
						}
					}
					bufferDePacotes.clear();
					bufferVazio = true;
				}
				if(pacote.isUltimo()) {
					end = true;
				}
			} else {
				if(!isInside(pacote, pacotesRecebidos)) {
					bufferDePacotes.add(pacote);
					if (bufferVazio) {
						bufferVazio = false;
					}
					// Mandando Nak do pacote que n�o foi recebido.
					Confirmacao objetoNak = new Confirmacao(pacoteEsperado, pacote.isUltimo(), false);
					byte[] nakBytes = Convert.toBytes(objetoNak);
					DatagramPacket pacoteNak = new DatagramPacket(nakBytes, nakBytes.length, pacoteRecebido.getAddress(), pacoteRecebido.getPort());
					if (Math.random() > PROBABILIDADE_DE_DESCARTE) {
						socketUdp.send(pacoteNak);
						System.out.println("Mandando NAK do pacote de numero " + pacoteEsperado+ " com  " + nakBytes.length  + " bytes.");
					} else {
						System.out.println("NAK do pacote de numero " + objetoNak.getNumDoPacote() + " foi perdido.");
					}
				}
			}
		}
		
		String result = null;
		StringBuilder stringBuilder = new StringBuilder("");
		for(Pacote p : pacotesRecebidos){
			for(byte b: p.getData()){
				stringBuilder.append((char) b);
			}
		}
		result = stringBuilder.toString();
		System.out.println("Dado enviado: " + result);
	}
	
	private static boolean isInside(Pacote pacote, ArrayList<Pacote> alvo) {
		for (int i = 0; i < alvo.size(); i++) {
			if(pacote.getNum() == alvo.get(i).getNum()){
				return true;
			}
		}
		return false;
	}
	
}
