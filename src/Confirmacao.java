import java.io.Serializable;

public class Confirmacao implements Serializable{
	
	private int numDoPacote;
	private boolean ultimo;
	private boolean isAck;

	public Confirmacao(int numDoPacote, boolean ultimo, boolean isAck) {
		super();
		this.numDoPacote = numDoPacote;
		this.ultimo = ultimo;
		this.isAck = isAck;
	}

	public int getNumDoPacote() {
		return numDoPacote;
	}

	public void setNumDoPacote(int packet) {
		this.numDoPacote = packet;
	}

	public boolean isUltimo() {
		return ultimo;
	}

	public void setUltimo(boolean ultimo) {
		this.ultimo = ultimo;
	}

	public boolean isAck() {
		return isAck;
	}

	public void setAck(boolean isAck) {
		this.isAck = isAck;
	}
}
