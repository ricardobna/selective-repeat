
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Semaphore;


public class Server {
	public static int QUANTIDADE_MAXIMA_DE_PACOTES = 5;
	public static int TAMANHO_DA_JANELA = 5;
	public final static int TEMPO_DE_TIMEOUT = 200;
	public static int LIMITE_DE_PACOTES_POR_SEGUNDO = 5;
	public static int PACOTES_LIVRES_PARA_SER_ENVIADOS = LIMITE_DE_PACOTES_POR_SEGUNDO;
	public static double PROBABILIDADE_DE_DESCARTE = 0.2;
	public static Semaphore semaforoMutex = new Semaphore(1);
	
	public static void main(String[] args) throws Exception{
		int tamanhoDoPacote = 0;
		int ultimoEnviado = 0;
		int pacoteEsperandoAck = 0;
		String dado; 
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite o dado a ser enviado: ");
		dado = scanner.nextLine();
		System.out.println("Diga a quantidade m�xima de pacotes a serem enviados: ");
		QUANTIDADE_MAXIMA_DE_PACOTES = scanner.nextInt();
		System.out.println("Diga a quantidade m�xima de pacotes a serem enviados por segundo para o cliente: ");
		PACOTES_LIVRES_PARA_SER_ENVIADOS = LIMITE_DE_PACOTES_POR_SEGUNDO = scanner.nextInt();
		System.out.println("Diga a quantidade de pacotes a serem enviados antes de receber a confirma��o de entrega: ");
		TAMANHO_DA_JANELA = scanner.nextInt();
		scanner.close();
		ArrayList<byte[]> bytesDoDado = new ArrayList<byte[]>();
		tamanhoDoPacote = dado.length()/QUANTIDADE_MAXIMA_DE_PACOTES;
		StringBuilder s = new StringBuilder("");
		s.append(dado);
		for(int i = 0; i < QUANTIDADE_MAXIMA_DE_PACOTES; i++) {
			int nextPos = i == QUANTIDADE_MAXIMA_DE_PACOTES - 1 ? s.length() : (i + 1)*(tamanhoDoPacote);
			String aux = s.substring(i*(tamanhoDoPacote), nextPos);
			bytesDoDado.add(aux.getBytes());
		}
		DatagramSocket socketUdp = new DatagramSocket();
		InetAddress enderecoDoCliente = InetAddress.getLocalHost();
		ArrayList<Pacote> enviados = new ArrayList<Pacote>();
		Thread threadDosPacotes = new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(1000);
						semaforoMutex.acquire();
						PACOTES_LIVRES_PARA_SER_ENVIADOS = LIMITE_DE_PACOTES_POR_SEGUNDO;
						semaforoMutex.release();
					} catch (InterruptedException e) {
						//N�o deve fazer nada caso seja interrompido.
					}
				}
			}
		});
		threadDosPacotes.start();
		while (true) {
			if(PACOTES_LIVRES_PARA_SER_ENVIADOS > 0 ) {
				while (ultimoEnviado < QUANTIDADE_MAXIMA_DE_PACOTES && TAMANHO_DA_JANELA > ultimoEnviado - pacoteEsperandoAck) {
					byte[] filePacketBytes = new byte[bytesDoDado.get(ultimoEnviado).length/QUANTIDADE_MAXIMA_DE_PACOTES];
					filePacketBytes = bytesDoDado.get(ultimoEnviado);
					Pacote objetoDoPacote = new Pacote(filePacketBytes, ultimoEnviado, (ultimoEnviado == QUANTIDADE_MAXIMA_DE_PACOTES - 1 ? true : false));
					byte[] pacoteASerEnviado = Convert.toBytes(objetoDoPacote);
					DatagramPacket pacote = new DatagramPacket(pacoteASerEnviado, pacoteASerEnviado.length, enderecoDoCliente, 35000);
					System.out.println("Mandando o pacote " + ultimoEnviado +  " com tamanho de " + pacoteASerEnviado.length + " bytes");
					enviados.add(objetoDoPacote);
					if(Math.random() > PROBABILIDADE_DE_DESCARTE) {
						socketUdp.send(pacote);
					} else {
						System.out.println("Falha ao enviar o pacote de numero " + objetoDoPacote.getNum() + " com " + pacoteASerEnviado.length + " bytes.");
					}
					ultimoEnviado++;
					semaforoMutex.acquire();
					PACOTES_LIVRES_PARA_SER_ENVIADOS--;
					semaforoMutex.release();
				}
			}
			byte[] bytesDeConfirmacao = new byte[100];
			DatagramPacket confirmacao = new DatagramPacket(bytesDeConfirmacao, bytesDeConfirmacao.length);
			try {// Seta o timeout para mandar a janela de novo.
				socketUdp.setSoTimeout(TEMPO_DE_TIMEOUT);
				socketUdp.receive(confirmacao);
				Confirmacao objetoDaConfirmacao = (Confirmacao) Convert.toObject(confirmacao.getData());
				if(objetoDaConfirmacao.isAck()) {
					System.out.println("ACK do pacote " + objetoDaConfirmacao.getNumDoPacote() + " recebido.");
					if ((objetoDaConfirmacao.getNumDoPacote() == QUANTIDADE_MAXIMA_DE_PACOTES || objetoDaConfirmacao.isUltimo()) && ultimoEnviado == QUANTIDADE_MAXIMA_DE_PACOTES){
						break;
					}
					pacoteEsperandoAck = Math.max(pacoteEsperandoAck, objetoDaConfirmacao.getNumDoPacote());
				} else {
					System.out.println("NAK do pacote " + objetoDaConfirmacao.getNumDoPacote() + " recebido.");
					Pacote pacote = new Pacote(bytesDoDado.get(objetoDaConfirmacao.getNumDoPacote()), objetoDaConfirmacao.getNumDoPacote(), objetoDaConfirmacao.isUltimo());
					byte[] bytesDoPacote = Convert.toBytes(pacote);
					DatagramPacket pacoteASerEnviado = new DatagramPacket(bytesDoPacote, bytesDoPacote.length, enderecoDoCliente, 35000 );
					socketUdp.send(pacoteASerEnviado);
					System.out.println("Reenviando o pacote de numero " + objetoDaConfirmacao.getNumDoPacote() +  " com tamanho de " + bytesDoPacote.length + " bytes.");
					semaforoMutex.acquire();
					PACOTES_LIVRES_PARA_SER_ENVIADOS--;
					semaforoMutex.release();
					
				}
			} catch (SocketTimeoutException e){
				if(PACOTES_LIVRES_PARA_SER_ENVIADOS > 0) {
					for (int i = pacoteEsperandoAck; i < ultimoEnviado; i++){
						byte[] pacoteASerEnviado = Convert.toBytes(enviados.get(i));
						DatagramPacket pacote = new DatagramPacket(pacoteASerEnviado, pacoteASerEnviado.length, enderecoDoCliente, 35000 );
						socketUdp.send(pacote);
						System.out.println("Reenviando o pacote de numero " + enviados.get(i).getNum() +  " com tamanho de " + pacoteASerEnviado.length + " bytes.");
						semaforoMutex.acquire();
						PACOTES_LIVRES_PARA_SER_ENVIADOS--;
						semaforoMutex.release();
					}
				}
			}
		}
		threadDosPacotes.stop();
		System.out.println("Transmiss�o finalizada.");
	}
	
}
